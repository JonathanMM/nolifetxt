<?php
$modif_ago = time() - filemtime('noair.xml');
if($modif_ago > 60*20)
//if(true)
{
    $doc = new DOMDocument();
    $doc->load( 'noair.xml' );

    $nodes = $doc->getElementsByTagName( "slot" );

    $pagesDispo = [601];

    $programmes = array(1 => array());
    $programmesDemain = array(11 => array());
    $iPage = 1;
    $iPageDemain = 11;
    $nLignes = 0;
    $nLignesDemain = 0;
    foreach($nodes as $ua) {
        $date = strtotime($ua->getAttribute("date"));
        if(date('Ymd') == date('Ymd', $date)) // Aujourd'hui
        {
            $nLignes++;
            if($nLignes >= 28)
            {
                $iPage++;
                $pagesDispo[] = 600+$iPage;
                $nLignes = 0;
            }
            $programmes[$iPage][] = $ua;
        }
        elseif(date('Ymd', time() + 86400) == date('Ymd', $date)) // Demain
        {
            $nLignesDemain++;
            if($nLignesDemain >= 28)
            {
                if($iPageDemain == 11)
                    $pagesDispo[] = 611;
                $iPageDemain++;
                $pagesDispo[] = 600+$iPageDemain;
                $nLignesDemain = 0;
            }
            $programmesDemain[$iPageDemain][] = $ua;
        }
    }

    $page = 620;
    $pages = array();
    $pagesProgrammes = array();
    foreach($programmes as $numPage => $progPages)
    {
        $pagesProgrammes[$numPage] = '';
        foreach($progPages as $prog)
        {
            $nom = (string) $prog->getAttribute("description");
            $type = (string) $prog->getAttribute("type");
            $heure = (string) substr($prog->getAttribute("date"), 11, 5);
            $pagesProgrammes[$numPage] .= $heure.' '.$nom;
            if($type != 'Clip')
            {
                $pages[$page] = array(
                    'titre' => (string) $prog->getAttribute("title"),
                    'soustitre' => (string) $prog->getAttribute("sub-title"),
                    'detail' => (string) $prog->getAttribute("detail"),
                    'heure' => $heure
                );
                $pagesDispo[] = $page;
                $pagesProgrammes[$numPage] .= '.......'.$page;
                $page++;
            }
            $pagesProgrammes[$numPage] .= "\n";
        }
    }
    foreach($programmesDemain as $numPage => $progPages)
    {
        $pagesProgrammes[$numPage] = '';
        foreach($progPages as $prog)
        {
            $nom = (string) $prog->getAttribute("description");
            $type = (string) $prog->getAttribute("type");
            $heure = (string) substr($prog->getAttribute("date"), 11, 5);
            $pagesProgrammes[$numPage] .= $heure.' '.$nom;
            /*if($type != 'Clip')
            {
                $pages[$page] = array(
                    'titre' => (string) $prog->getAttribute("title"),
                    'soustitre' => (string) $prog->getAttribute("sub-title"),
                    'detail' => (string) $prog->getAttribute("detail"),
                    'heure' => $heure
                );
                $pagesDispo[] = $page;
                $pagesProgrammes[$numPage] .= '.......'.$page;
                $page++;
            }*/
            $pagesProgrammes[$numPage] .= "\n";
        }
    }
    $stockage = array(
        'programme' => $pagesProgrammes,
        'details' => $pages,
        'pages' => $pagesDispo
    );

    $fichier = fopen('noair.json', 'w+');
    fwrite($fichier, json_encode($stockage));
    fclose($fichier);
}

	function objectToArray($d) {
		if (is_object($d)) {
			// Gets the properties of the given object
			// with get_object_vars function
			$d = get_object_vars($d);
		}

		if (is_array($d)) {
			/*
			* Return array converted to object
			* Using __FUNCTION__ (Magic constant)
			* for recursive call
			*/
			return array_map(__FUNCTION__, $d);
		}
		else {
			// Return array
			return $d;
		}
	}

if(isset($_GET['page']))
{
    $brut = file_get_contents('noair.json');
    $infos = objectToArray(json_decode($brut));
    $numPage = intval($_GET['page']);
    if($numPage == 0)
        echo json_encode($infos['pages']);
    elseif($numPage > 600 && $numPage < 620)
        echo $infos['programme'][$numPage - 600];
    elseif($numPage >= 620 && $numPage < 700)
        echo json_encode($infos['details'][$numPage]);
}
?>
