<?php
if(isset($_GET['page']))
{
    if($_GET['page'] == '101%')
    {
        echo '101% est le magazine quotidien de Nolife, diffusé du mardi au vendredi à 19 heures. D\'une durée moyenne de 25 minutes, il s\'agit d\'une émission multi-thématique : on y retrouve ainsi quatre ou cinq rubriques selon les jours, pouvant être consacrées aux jeux vidéo, à la culture japonaise ou bien à toute autre thème traité sur la chaîne.

101% est présenté en alternance par plusieurs présentateurs réguliers

    Saisons 1 à 5 : Davy Mourier et Benoît Legrand
    Saisons 6 et 7 :Benoît Legrand et Philippe Nègre
    Saisons 8 et 9 : Benoît Legrand, Philippe Nègre et Davy Mourier
    Saisons 10 et 11 : Davy Mourier, Benoît Legrand et Mima
    Saisons 12 à 15.1 : Davy Mourier, Benoît Legrand et Claire
    Saison 15.2 : Davy Mourier, Benoît Legrand et Aurore
    Depuis la saison 16.1 : Davy Mourier, Paul Bourrières et Aurore

La présentation du 101% du vendredi est quand à elle laissée à n\'importe qui : personnalités de la communauté geek, artistes japonais de passage en France, membres du staff de la chaîne... se succèdent donc à la présentation ce jour là.

Originellement diffusé du lundi au vendredi, 101% laisse sa place le lundi, depuis la rentrée de septembre 2015, à 101 PUR 100, déclinaison extend de 101% en direct.

La sous-page présentateurs indique le nombre de 101% de chaque présentateur pour chaque saison

Fiche technique

    Format : 16/9 (4/3 pour les saisons 1 à 3), HD depuis septembre 2013
    Réalisation : Alex Pilot
    Supervision et assemblage : Sylvain Dreyer
    Présentation : Davy Mourier, Benoît Legrand, Philippe Nègre, Mima, Aurore N\'importe qui (le vendredi) et avec quelques invités
    Musique (génériques et jingles) : Akira Yamaoka
    Mixage : Mathilde Wasilewski
    Habillage : Olivier Rocques
    Speaks : Thomas Guitard, Geneviève Doang, Sébastien Ruchet, Julien Pirou...
    Logos évolutifs grâce au programme Logolife : Pixelphil (depuis la saison 3)

Structure de 101%

Quatre ou cinq rubriques sont diffusées dans chaque 101%. Sauf exceptions, les news sont diffusées quotidiennement. Trois ou quatre critiques de jeu sont également diffusées durant la semaine.

Les autres rubriques sont hebdomadaires, bimensuelles ou, plus exceptionnellement, diffusées plusieurs fois dans la semaine.

Conducteur de l\'émission

Chaque 101% est composé de quatre rubriques et de cinq plateaux ou de cinq rubriques et de six plateaux. Ce programme type est néanmoins modifié lors d\'événements spéciaux (rubriques absentes, couverture d\'un évènement...). À partir de la saison 12, l\'animateur choisit l\'ordre des rubriques du 101% qu\'il présente. De plus, ses plateaux ne présentent pas toujours la rubrique qui va suivre, et le présentateur peut préférer nous parler d\'un sujet qui l\'intéresse ou présenter un sketch à suivre sur l\'ensemble des plateaux (voire sur plusieurs 101%).

101% fait environ 25 minutes mais cette durée n\'est qu\'une valeur moyenne car si un sujet nécessite plus (ou moins de temps) que la durée moyenne, elle durera plus (ou moins) de temps que prévu, et la durée totale du 101% en sera modifiée. Ainsi, le plus court 101%, diffusé le 21 avril 2009, a duré 9 minutes et 43 secondes, bien que certains considère que le 101% le plus court est le 101% n°86 (26/11/2007), d\'une durée de 59 secondes, diffusé pendant les grèves des transports de novembre 2007. Le plus long 101%, en faisant exception du 1001ème numéro diffusé durant la soirée des cinq ans, est quand à lui celui diffusé le 20 juin 2011 et ayant duré 58 minutes et 48 secondes.

Sommaire de 101% selon les saisons et les jours de la semaine

Le sommaire de 101% change à chaque saison. Chaque rubrique possède son propre logo, avec une couleur et une barre de progression indépendante du reste du 101%. Depuis le lundi 2 juin 2008, un logo 101% avec une flèche est situé en haut à droite de l\'écran, ce logo indique quand à lui l\'avancée dans l\'émission dans sa globalité. Ce logo a été modifié au début de la saison 12.

Habillage

L\'habillage de l\'émission est réalisé par Olivier Rocques alias Monsieur cubes et flèches et est généralement renouvelé à chaque nouvelle rentrée, au mois de septembre.

Le générique

Le générique original de l\'émission débute par une chute de cubes au sol et desquels surgit la flèche, qui navigue ensuite entre les cubes en mouvement jusqu\'à révéler le nom 101%. Cette thématique est déclinée lors des saisons suivantes, les cubes étant même remplacés par des sphères reliées par trois lors des saisons 12 et 13.1.

Le plateau

L\'émission était tournée durant les premières saisons sur un fond blanc, qui était conservé dans l\'émission. Durant les saisons 4 et 5, les cubes sont présents : deux piles de cubes, à gauche et à droite de l\'écran, montent et descendent successivement. À partir de la saison 6, l\'émission est tournée sur fond vert, ce qui permet une incrustation des cubes plus poussée : on voit des vagues de cubes avancer au loin, ainsi que la flèche qui tourne autour et qui passe devant le présentateur de l\'émission. Ce système sera repris avec quelques modifications pour le plateau des saisons suivantes. Comme pour le générique, les cubes sont remplacés par des sphères pour les saisons 12 et 13.1.

Les virgules

Les virgules séparent les rubriques des plateaux qui les suivent et présentent elles aussi des cubes ou des sphères en mouvement selon les saisons. La couleur de ces virgules est différente selon les saisons.

Habillage sonore

La musique du générique ainsi que celles des différents jingles utilisés dans 101% ont été réalisées par Akira Yamaoka. Ces musiques ont été modifiées au cours de la saison 7 puis à chaque rentrée de septembre depuis la saison 12.

La musique du générique de fin est changée à chaque nouvelle saison.

    Saison 1 : reprise de la musique du générique
    Saisons 2 et 3 : Mary France de Jean-Jacques Perrey
    Saison 4 : Thank you for the music de Cornelius
    Saison 5 : silver snow, shivering soul de the guitar plus me
    Saison 6 : Yuki ga furu machi de Puffy
    Saison 7 : Labyrinth de Bahashishi
    Saison 8 : Meiro Game de Quruli / Perfect Star Perfect Style de Perfume
    Saison 9 : Libera in Sleep de Sakamoto Miu
    Saison 10 : Wanka de Murmur
    Saison 11 : Et Maintenant de Ginjirô Sweet
    Saison 12 : Dream about... de Sawa
    Saison 13.1 : shine UP de SPOON+
    Saison 13.2 : Aozora de MIWA
    Saison 14.1 : M de FUKI
    Saison 14.2 : TENKI AME de Manu
    Saison 15.1 : Fukai Mori de Sekai no Owari
    Saison 15.2 : Mushi No Onna de TOGAWA JUN
    Saison 16.1 : Chizu wo kudasai de YUSA Mimori ';
        exit();
    }
    $remplacement = function($array)
    {
        $pos = strpos($array[1], '|');
        if($pos === false)
            return $array[1];
        else
            return substr($array[1], $pos+1);
    };

    $remplacement_tab = function($array)
    {
        $split = explode("\n", $array[0]);
        $retour = '';
        foreach($split as $ligne)
        {
            if(substr($ligne, 0, 2) == '{{')
                $retour .= $ligne."\n";
        }
        return trim($retour);
    };

    $xml = simplexml_load_file('http://www.nolife-wiki.fr/api.php?action=query&titles='.urlencode($_GET['page']).'&export&exportnowrap&format=xml');
    $content = (string) $xml->page->revision->text[0];
    //{{Episode|6064|4:54|B2|01/10/2010| 1|Mythos - Tera - Final Fantasy XIV - Star Wars The Old Republic - Guild Wars 2 }}
    $content = preg_replace_callback("#\{\|(.*)*\|\}#Uis", $remplacement_tab, $content);
    $array = array(
        "#\{\{Episode\|(.*)\|(.*)\|(.*)\|(.*)\|(.*)\|(.*)\}\}#Uis",
        "#\{\{(.*)\}\}#Uis",
        "#'''(.*)'''#Uis",
        "#''(.*)''#Uis",
        "#={4}(.*)={4}#Uis",
        "#={3}(.*)={3}#Uis",
        "#={2}(.*)={2}#Uis",
        "#\[\[Catégorie:(.*)\]\]#Uis"
    );

    $array2 = array(
        '#$5 : $6 ($4 - $2)',
        '',
        '$1',
        '$1',
        '$1',
        '$1',
        '$1',
        ''
    );
    $content = preg_replace($array, $array2, $content);
    $content = preg_replace_callback("#\[\[(.*)]\]#Uis", $remplacement, $content);
    $content = preg_replace("#\[(.*) (.*)\]#Ui", '$2', $content);
    $content = str_replace('<br>', ' ', $content);
    echo trim($content);
}
?>
