$(function() {
	showHeure();

	var nextPage = 0;

	var pages = {
		100: "index",
		101: "info1",
		102: "info2",
		103: "info3",
		110: "nolife",
		111:"Nolife",
		112:"Saisons",
		113:"Publicité",
		114:"Pocket Shami",
		115:"Site",
		116:"Musiques",
		117:"CSA",
		118:"Logo",
		121:"Nolife ∞",
		122:"Nolife no Lacryma",
		123:"Nolife no thema",
		124:"Nolife Story",
		125:"Otaku2Vice",
		126:"Acid+ LE antoinette",
		130: "noco",
		131:"noco",
		132:"Catégorie:Catalogue Nolife",
		133:"Catégorie:Catalogue Guardians",
		134:"Catégorie:Catalogue LBL42",
		135:"Catégorie:Catalogue Olydri",
		136:"Catégorie:Catalogue Toki Media",
		137:"Catégorie:Catalogue Wakanim",
		138:"Catégorie:Catalogue WE Productions",
		139:"Nolife Online",
		200: "programmes",
		201: "programmes2",
		202: "programmes3",
		210:"101%",
		211:"(Vous savez) Pourquoi on est là",
		212:"1 minute pour parler de...",
		213:"1D6",
		214:"2 minutes pour parler de...",
		215:"À lire, à voir",
		216:"BD Blogueurs",
		217:"Big Bang Hunter",
		218:"Big Bug Hunter",
		219:"Catsuka",
		220:"Ce soir j'ai raid",
		221:"Compiler",
		222:"Critique de jeu",
		223:"Crunch Time",
		224:"Debug Mode",
		225:"Double Face",
		226:"Fatal Misses & Plastic Girls‎",
		227:"Game Center",
		228:"Hidden Palace",
		229:"La Faute à l'algo",
		230:"La minute du geek",
		231:"Les jeux téléchargeables",
		232:"Les oubliés de la Playhistoire",
		233:"Metal Missile & Plastic Gun",
		234:"Mon Plan Culte",
		235:"Mon souvenir",
		236:"Money Shot",
		237:"Mot de saison",
		238:"News",
		239:"Nochan",
		240:"Nolife Emploi IRL",
		241:"Oscillations",
		242:"OTO",
		243:"OTO Play Sample",
		244:"Périscope",
		245:"Pico Pico",
		246:"PIXA",
		247:"Reportage",
		248:"Retro & Magic",
		249:"Roadstrip Light",
		250:"Smartphones & tablettes",
		251:"Sorties animes et mangas",
		252:"Stamina",
		253:"Superplay",
		254:"Technologie de l'Information en Pratique et Sans danger",
		255:"Temps perdu",
		256:"Tōkyō Café",
		257:"toco toco",
		258:"Very Hard",
		259:"Vue Subjective Mini",
		260:"101 PUR 100",
		261:"Actu J-Music",
		262:"Ce qu'il ne faut pas dire",
		263:"Club Télé Achier",
		264:"Le saviez-tu ? de Jean Foutre",
		265:"L'hippodrôle de Nolife",
		266:"The Place to Be",
		271:"Ami Ami Idol : Hello ! France",
		272:"C’est mon vote qui passe ce soir sur Nolife",
		273:"J-Top",
		274:"Le Magazine des Indies",
		275:"Mogura Musique",
		276:"Nolife Plugged",
		277:"OTO Ex",
		278:"War Pigs",
		281:"3 minutes pas + pour parler d'un jeu 3+",
		282:"Chaud Time",
		283:"Chez Marcus",
		284:"Classés 18+",
		285:"EXP",
		286:"Extra Life",
		287:"FAQ",
		288:"Game Life",
		289:"Hall of Shame",
		290:"Hard Corner",
		291:"Jeu-Top",
		292:"L'Hebdo Jeu Vidéo",
		293:"OTO Play",
		294:"Skill",
		295:"Superplay Ultimate",
		301:"56Kast",
		302:"Ecrans.fr le podcast",
		303:"J'irai loler sur vos tombes",
		304:"Périscope 2",
		311:"Côté Comics",
		312:"Le duo comics",
		313:"Roadstrip",
		321:"Courts-métrages",
		322:"Format Court",
		323:"Rêves et Cris",
		330:"Esprit Japon",
		332:"Japan in Motion",
		333:"Kira Kira Japon",
		334:"Kyary Pamyu Pamyu Telebi John !",
		335:"Kyô no wanko",
		336:"Nihongo Ga Dekimasu Ka ?",
		341:"L'instant Kamikaze",
		342:"Le Golden Show",
		343:"Prélude en lol bémol",
		351:"Le point sur Nolife",
		352:"Les news de Nolife Online",
		361:"101% c'était mieux avant",
		362:"Documentaire",
		363:"Le coin des abonnés",
		364:"Nolife Online Collection",
		365:"Oldies Nolife",
		366:"Random Access Archives",
		367:"Replay Value",
		368:"The Incredible Horror Show",
		371:"À table !",
		372:"Costugrale",
		373:"Costume Player",
		374:"Le jeu du ***",
		375:"Mange mon geek",
		376:"Temps réel",
		381:"100% Légal",
		382:"Attic Attic Attic : Aïe Aïe Aïe",
		383:"Extra Nolife",
		384:"Jeu-Top annuel",
		385:"J-Music Ultra Combo playlist",
		386:"Le Calendrier de l’Avent après 101%",
		387:"Le J-Top de l'année",
		388:"Les clips de l'année",
		389:"Les immanquables de l'année",
		390:"Les immanquables de 2013",
		391:"Les immanquables de 2014",
		392:"Les secondes de remplissage de Medoc Elmedoc, provider d’entertainment",
		393:"OTO Reverse",
		394:"Rush Mode",
		395:"Soirée spéciale",
		396:"Space Hyper UFO Media DJ",
		397:"The Game : l'expérience",
		398:"Xmas Life",
		410: "series",
		411:"Another Hero",
		412:"CréAtioN",
		413:"Devil'Slayer",
		414:"Flander's Company",
		415:"France Five",
		416:"Geek's Life",
		417:"J'ai jamais su dire non",
		418:"Karaté Boy",
		419:"La Dernière Série avant la Fin du Monde",
		420:"Lazy Puppy",
		421:"Le Blog de Gaea",
		422:"Le Visiteur du Futur",
		423:"Les Blablagues de Laurent-Laurent",
		424:"Les conseils vidéo du professeur Théorie et du docteur Pratique",
		425:"Metal Hurlant Chronicles",
		426:"Mon Nolife à Moi",
		427:"NerdZ",
		428:"NONSÉRIE",
		429:"Noob",
		430:"Pendant ce temps... à la rédac",
		431:"Purgatoire",
		432:"SPACE",
		433:"The Guild",
		434:"WarpZone Project",
		435:"WiP",
		440: "animes",
		441:"Chikyû shôjo Arjuna",
		442:"Animation Runner Kuromi",
		443:"City Hunter : Bay City Wars",
		444:"City Hunter : Complot pour 1 million de dollars",
		445:"Rurōni Kenshin : Seisōhen",
		446:"Rurōni Kenshin : Tsuiokuhen",
		447:"Haibane Renmei - Ailes Grises",
		448:"Berserk",
		449:"City Hunter 3",
		450:"City Hunter 91",
		451:"s-CRY-ed",
		452:"Top o Nerae! - Gunbuster",
		453:"Top o Nerae 2! - Diebuster",
		454:"Eyeshield 21",
		455:"Kare Kano - Entre elle et lui",
		456:"Windy Tales",
		457:"Fuliculi",
		458:"RahXephon",
		459:"La Mélancolie de Haruhi Suzumiya",
		460:"Que sa volonté soit faite",
		461:"Paranoia Agent",
		462:"Samurai Champloo",
		463:"Innocent Venus",
		464:"The Tower of Druaga",
		465:"Eden of the East",
		466:"Another",
		467:"Black★Rock Shooter",
		468:"Kids on the Slope",
		469:"Deadman Wonderland",
		470:"FLAG",
		471:"Jyu Oh Sei",
		472:"Princess Jellyfish",
		473:"C-Control",
		474:"Fractale",
		475:"Casshern Sins",
		476:"Bakemonogatari",
		477:"Blood Lad",
		478:"Nadia, le secret de l'Eau Bleue",
		479:"Tsuritama",
		480:"Free!",
		481:"Noir",
		482:"Gundam G no Reconguista",
		483:"Wolf's Rain",
		484:"Koitabi",
		500: "staff1",
		501: "staff2",
		502:"Sébastien Ruchet",
		503:"Alex Pilot",
		504:"Asaoka Suzuka",
		505:"Cyril Lambin",
		506:"Moguri",
		511:"Anne Ferrero",
		512:"Aurore",
		513:"Aymeric Ramanakasina",
		514:"Camille Gévaudan",
		515:"Davy Mourier",
		516:"Emmanuel Pettini",
		517:"Erwan Cario",
		518:"Florent Gorges",
		519:"Frédéric Hosteing",
		520:"Jean-Marc Imbert",
		521:"Josaudio",
		522:"Julien Pirou",
		523:"Lilian Michelon",
		524:"Tsuka",
		525:"Marcus",
		526:"Medoc",
		527:"Nicolas Robin",
		528:"Parotaku",
		529:"Pascaline",
		530:"Paul Bourrières",
		531:"Sironimo",
		532:"Slimane-Baptiste Berhoun",
		541:"Angéla Nichon",
		542:"Aurélien Louvet",
		543:"Brian Martin",
		544:"Geneviève Doang",
		545:"Guillaume Boschini",
		546:"Jonathan A. Casses",
		547:"Laurine Bollon",
		548:"Olivier Rocques",
		551:"Anh Hoà Truong",
		552:"Benoît Legrand",
		553:"Benzaie",
		554:"Caroline Segarra",
		555:"Claire",
		556:"Clément Maurin",
		557:"DamDam",
		558:"Darkwam",
		559:"Didier Richard",
		560:"Dr Lakav",
		561:"Radigo",
		562:"Macha Lobanova",
		563:"Mathilde Wasilewski",
		564:"Mima",
		565:"Mr Poulpe",
		566:"Philippe Nègre",
		567:"Pierre-Alexandre Conte",
		568:"Rémy Argaud",
		569:"Ruddy Pomarede",
		570:"Sébastien Ciot",
		571:"Zed",
		572:"Thierry Falcoz",
		573:"Thomas Guitard",
		600: "grille",
		801: "pub1",
		802: "pub2",
		803: "pub3",
		804: "pub4",
		805: "pub5",
		806: "pub6",
		888: "credit"
	};

	getPageProgramme();

	var nonwikiPages = [100, 101, 102, 103, 110, 130, 200, 201, 202, 410, 440, 500, 501, 600, 801, 802, 803, 804, 805, 806, 888];

	var nbEcrans = 1;
	var fullContentPage = null;
	var ecranActuel = 1;
	var dataRecue = true;
	var dataPourPage = null;

	var pagesPossibles = Object.keys(pages);

	var timeout = null;
	var intervalEcran = null;

	showPage(100);

	function getPageProgramme()
	{
		$.get('noair.php?page=0', function(data) {
			var tableauPage = JSON.parse(data);
			tableauPage.forEach(function(item) {
				var p = parseInt(item);
				if(p <= 610)
					pages[p] = 'Aujourd\'hui ('+(p-600)+')';
				else if(p < 620)
					pages[p] = 'Demain ('+(p-610)+')';
				else
					pages[p] = 'Détails ('+(p-619)+')';
			});
			pagesPossibles = Object.keys(pages);
			pagesPossibles.sort();
		});
	}

	function showHeure() {
		date = new Date;
		h = date.getHours();
		if(h<10)
		{
			h = "0"+h;
		}
		m = date.getMinutes();
		if(m<10)
		{
			m = "0"+m;
		}
		s = date.getSeconds();
		if(s<10)
		{
			s = "0"+s;
		}
		$('#heure').html(h+":"+m+":"+s);
		setTimeout(showHeure,'1000');
	}

	$('html').keypress(function( event ) {
		if(event.which >= 48 && event.which<=57) {
			input = event.which-48;
			nextPage = 10*nextPage+input;
			if(timeout != null)
			{
				clearTimeout(timeout);
				timeout = null;
			}
			if(nextPage<100)
				$('#page').html('P'+nextPage+'▮');
			else
			{
				$('#page').html('P'+nextPage);
				runCountdown(nextPage);
				nextPage=0;
			}
		} else if(event.keyCode == 38 || event.keyCode == 40) // ↑ ou ↓
		{
			var n = pagesPossibles.length;
			var i = pagesPossibles.indexOf($('#page').html().substr(1));
			if(event.keyCode == 38)
				i += 1;
			else
				i -= 1;

			if(i < 0)
				i = n-1;
			else if(i >= n)
				i = 0;

			nextPage = parseInt(pagesPossibles[i]);
			if(timeout != null)
			{
				clearTimeout(timeout);
				timeout = null;
			}
			$('#page').html('P'+nextPage);
			runCountdown(nextPage);
			nextPage=0;
		}
	});

	function runCountdown(nextPage)
	{
		_paq.push(['trackEvent', 'Page', nextPage]);
		if(pages[nextPage] && nonwikiPages.indexOf(nextPage) == -1) //On va demander les données dès maintenant
		{
			dataRecue = false;
			if(nextPage < 600 || nextPage > 699)
			{
				var urlWiki = './wiki.php?page='+encodeURIComponent(pages[nextPage]);
				$.get(urlWiki, function( data ) {
					dataRecue = true;
					dataPourPage = data;
				});
			} else {
				var urlWiki = './noair.php?page='+nextPage;
				$.get(urlWiki, function( data ) {
					dataRecue = true;
					if(nextPage < 620)
						dataPourPage = data;
					else
					{
						var infos = JSON.parse(data);
						dataPourPage = 'Titre : '+infos.titre+"\n"+
							infos.soustitre+"\n"+
							'Diffusion : '+infos.heure+"\n"+
							infos.detail;
					}
				});
			}
		}
		var iCherche = pagesPossibles.indexOf(''+nextPage);
		var n = pagesPossibles.length;
		var iRand = iCherche;
		while(iCherche == iRand)
		{
			iRand = Math.floor(Math.random() * n);
			if(iRand >= n)
				iRand = n - 1;
		}
		timeout = setTimeout(nextCountdown, 50, iRand, iCherche, nextPage);
	}

	function nextCountdown(i, finish, page)
	{
		var n = pagesPossibles.length;
		i -= 1;
		if(i < 0)
			i = n - 1;
		$('#compteur').html(pagesPossibles[i]);
		if(dataRecue && i == finish)
			showPage(page);
		else
			timeout = setTimeout(nextCountdown, 50, i, finish, page);
	}

	function showPage(page)
	{
		if(pages[page])
		{
			if(intervalEcran != null)
			{
				clearInterval(intervalEcran);
				intervalEcran = null;
			}
			$('.page').each(function(){
				$(this).css('display', 'none');
			});
			if(nonwikiPages.indexOf(page) == -1)
			{
				showWikiPage(pages[page]);
				$('#page_wiki').css('display', 'block');
				if(page>200&&page<400)
					$('#retourCategorieWiki').html('PROGRAMMES......................<span class="pageNumber">200</span>');
				else if(page>400&&page<440)
					$('#retourCategorieWiki').html('SÉRIES..........................<span class="pageNumber">410</span>');
				else if(page>440&&page<500)
					$('#retourCategorieWiki').html('ANIMES..........................<span class="pageNumber">450</span>');
				else if(page>500&&page<600)
					$('#retourCategorieWiki').html('STAFF...........................<span class="pageNumber">500</span>');
				else if((page>600&&page<610)||(page>610&&page<620))
				{
					if(pages[page+1])
						$('#retourCategorieWiki').html('SUITE...........................<span class="pageNumber">'+(page+1)+'</span>');
					else
						$('#retourCategorieWiki').html('');
				}
				else if(page>=620&&page<700)
					$('#retourCategorieWiki').html('GRILLE..........................<span class="pageNumber">600</span>');
				else
					$('#retourCategorieWiki').html('');
			}
			else
			{
				$("#compteurPage").html('1/1');
				$('#page_'+pages[page]).css('display', 'block');
			}
		}
		nextPage=0;
	}

	function showWikiPage(page)
	{
		$("#wikiTitre").html(page);
		$("#wikiContent").html('');
		//urlWiki = './wiki.php?page='+encodeURIComponent(page);
		//$.get(urlWiki, function( data ) {
		//var contenu = data.replace(/\n/g, '<br />');
		var nbLignes = 1;
		var nbChar = 0;
		nbEcrans = 1;
		var ecranContenu = '';
		var maxLigne = 30;
		var maxChar = 88;
		fullContenuPage = [];
		for (var c of dataPourPage) {
			if(c == "\n" || c == "\r")
			{
				nbLignes++;
				nbChar = 0;
				if(nbLignes >= maxLigne)
				{
					fullContenuPage[nbEcrans-1] = ecranContenu;
					ecranContenu = '';
					nbEcrans++;
					nbLignes = 0;
				} else {
					ecranContenu += '<br />';
				}
			}
			else {
				nbChar++;
				if(nbChar >= maxChar)
				{
					nbLignes++;
					if(nbLignes >= maxLigne)
					{
						fullContenuPage[nbEcrans-1] = ecranContenu;
						ecranContenu = c;
						nbEcrans++;
						nbLignes = 0;
					} else {
						ecranContenu += '<br />'+c;
					}
					nbChar = 1;
				} else
					ecranContenu += c;
			}
		}
		fullContenuPage[nbEcrans-1] = ecranContenu;
		if(nbEcrans == 1)
			$("#wikiContent").html(ecranContenu);
		else
		{
			$("#wikiContent").html(fullContenuPage[0]);
			ecranActuel = 1;
			$("#compteurPage").html(ecranActuel+'/'+nbEcrans);
			intervalEcran = setInterval(changeEcran, 15000);
		}
		//});
	}

	function changeEcran()
	{
		ecranActuel++;
		if(ecranActuel > nbEcrans)
			ecranActuel = 1;
		$("#wikiContent").html(fullContenuPage[ecranActuel-1]);
		$("#compteurPage").html(ecranActuel+'/'+nbEcrans);
	}
})
