<!DOCTYPE html>
<html>
	<head>
		<title>Nolife - Télétexte</title>
		<meta charset="utf-8"/>
		<script type="text/javascript" src="https://code.jquery.com/jquery-2.2.2.min.js"></script>
		<link href='teletexte.css' type='text/css' rel='stylesheet' />
	</head>
	<body>
		<?php
		$modif_ago = time() - filemtime('noair.xml');

		if($modif_ago > 60*20)
		{
			$url = 'http://www.nolife-tv.com/noair/noair.xml';
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_USERAGENT, 'NoAir');
			$resultat = curl_exec ($ch);
			curl_close($ch);

			$fichier = fopen('noair.xml', 'w+');
			fwrite($fichier, $resultat);
			fclose($fichier);
		}

		$doc = new DOMDocument();  
		$doc->load( 'noair.xml' );  

		$nodes = $doc->getElementsByTagName( "slot" );  
		$current=null;
		$next=null;

		foreach($nodes as $ua) {  
			if(strtotime($ua->getAttribute("date"))-time()>0) 
			{
				$next=$ua;
				break;

			}
			else
			{
				$current=$ua;
			}
		} 

		$progNow = $current->getAttribute("title");
		$progNext = $next->getAttribute("title");
		$heureNext = substr($next->getAttribute("date"), 11, 5);
		if(strlen($progNow)>35)
			$progNow=substr($progNow, 0, 32).'...';
		if(strlen($progNext)>35)
			$progNext=substr($progNext, 0, 32).'...';
		?>
		<span id="page">P100</span>
		<span id="compteur">100</span>
		<span id="compteurPage">1/1</span>
		<span id="nolifeTXT">NOLIFE-TXT</span>
		<span id="heure">00:00:00</span>
		<div id="screen">
			<div id="page_index" class="page">
				<img id="logo" src="http://www.nolife-wiki.fr/images/a/ac/LogoP_noir.png"/>
				<span id="slogan">Y a pas que la vraie vie dans la vie !</span>
				<span id="antenne">
					<span id="antenneNowLabel" class="prog textLine">à l'antenne</span>
					<span id="antenneNow" class="prog textLine"><?php echo $progNow;?></span>
					<span id="antenneNextLabel" class="prog textLine">à <?php echo $heureNext;?></span>
					<span id="antenneNext" class="prog textLine"><?php echo $progNext;?></span>
				</span>
				<span id="antenneShadow"></span>
				<span id="infos">
					<span id="info1" class="info textLine">Un présentateur de 101% s'exhibe à l'antenne<span class="pageNumber">101</span></span>
					<span id="info2" class="info textLine">Audiences : Nolife regardée par 895 personnes<span class="pageNumber">102</span></span>
					<span id="info3" class="info textLine">Suisse : grève générale<span class="pageNumber">103</span></span>
				</span>
				<span id="pub1" class="pub textLine">LE J-TOP, FAUT QUE J'Y VOTE ! www.nolife-tv.com/jmusic<span class="pageNumber">801</span></span>
				<span id="pub2" class="pub textLine"><span class="blink">OFFRE SPÉCIALE NOUVEAUX VOTANTS : LE 1er SLOT À 10 CAILLOUX !</span><span class="pageNumber">802</span></span>
				<span id="pub3" class="pub textLine">REJOINS LES PETITS AMIS DE NOLIFE<span class="pageNumber">803</span></span>
				<span id="plus">
					<span class="plus l1 c1 textLine">NOLIFE............<span class="pageNumber">110</span></span>
					<span class="plus l1 c2 textLine">NOCO..............<span class="pageNumber">130</span></span>
					<span class="plus l2 c1 textLine">PROGRAMMES........<span class="pageNumber">200</span></span>
					<span class="plus l2 c2 textLine">SÉRIES............<span class="pageNumber">410</span></span>
					<span class="plus l2 c3 textLine">ANIMÉS............<span class="pageNumber">440</span></span>
					<span class="plus l3 c1 textLine">L'ÉQUIPE..........<span class="pageNumber">500</span></span>
					<span class="plus l4 c1 textLine">GRILLE............<span class="pageNumber">600</span></span>
				</span>
				<span id="pub4" class="pub textLine">LES VIDÉOS EN LIGNE www.noco.tv<span class="pageNumber">804</span></span>
				<span id="pub5" class="pub textLine"><span class="blink">ALLER AU JAPON POUR PAS CHER !</span><span class="pageNumber">805</span></span>
				<span id="pub6" class="pub textLine">SOLITUDE ? DEMANDE CONSEIL À TONTON MEDOC<span class="pageNumber">806</span></span>
			</div>
			<div id="page_info1" class="page">
				<div class="newsBar">FRANCE > MÉDIAS</div>
				<div class="newsTitle">Un présentateur de 101% s'exhibe à l'antenne</div>
				<div class="newsContent">
					Stupeur lors de la diffusion du 101% du 29 mars dernier. Le présentateur de l'émission, Paul Bourrières, s'est en effet exhibé à l'image quelques minutes après avoir, toujours devant les caméras, consommé une substance visiblement hallucinogène.<br/><br/>
					Une séquence qui n'a pas choqué les téléspectateurs, bien au contraire, ces derniers ayant félicité l'animateur qui s'était déjà fait remarquer la semaine précédente en ayant envoyé 14 personnes aux urgences suite à ses explications des théories de voyage temporel.<br/><br/>
					Contacté par nos soins, Olivier Schrameck, président du Conseil Supérieur de l'Audiovisuel (CSA), ne trouve rien à redire non plus : «Franchement, comparé aux autres chaînes, c'est même encore un peu trop léger».
				</div>
				<span class="retourAccueil">INDEX...........................<span class="pageNumber">100</span></span>
			</div>
			<div id="page_info2" class="page">
				<div class="newsBar">FRANCE > MÉDIAS</div>
				<div class="newsTitle">Audiences : Nolife regardée par 895 personnes</div>
				<div class="newsContent">
					«Un grand ouf de soulagement». C'est la première réaction de Sébastien Ruchet, président de la chaîne de télévision Nolife, au communiqué de Médiamétrie annonçant la correction d'une erreur publiée dans les derniers résultats d'audience Mediamat'Thematik.<br/><br/>
					Le communiqué du 10 mars 2016 annonçait 895.000 téléspectateurs mensuels pour la chaîne thématique mais l'erreur était trop grossière pour passer inaperçue : ce sont en effet 895 personnes qui regardent Nolife chaque mois. «On a quand même une réputation à tenir», poursuit M. Ruchet. «Ce n'est pas parce que Benoît est parti que l'on a envie de faire de l'audience. Et puis quoi après, gagner de l'argent, tant qu'on y est ?».
				</div>
				<span class="retourAccueil">INDEX...........................<span class="pageNumber">100</span></span>
			</div>
			<div id="page_info3" class="page">
				<div class="newsBar">EUROPE > SUISSE</div>
				<div class="newsTitle">Suisse : grève générale</div>
				<div class="newsContent">
					La Confédération Helvétique est désormais à l'arrêt. L'ensemble des syndicats ont en effet lancé une grève générale à durée illimitée, soutenue par la police, l'armée, le CIO et Axel Lipot, retraité cruciverbiste de 64 ans.<br/><br/>
					Toutes ces personnes se retrouvent dans la rue avec un seul but : pouvoir enfin regarder Nolife sur les écrans de télévision suisses. Coeur du mouvement de par sa situation géographique, Genève a été l'hôte de la plus grosse manifestation du mouvement, avec un défilé de 190.000 personnes, soit 95% de la population de la ville.<br/><br/>
					«Jusque là, on disait rien, on savait la chaîne dans une situation difficile. Mais maintenant qu'on sait qu'ils cherchent des partenaires, on veut les aider, tout de suite !» précise un manifestant, banquier de profession. «Et puis moi, j'aimerais que les gens arrêtent de me briser les tablettes en pensant que je suis Alex Pilot», renchérit M. Lipot.
				</div>
				<span class="retourAccueil">INDEX...........................<span class="pageNumber">100</span></span>
			</div>
			<div id="page_pub1" class="page">
				<div class="pubTitre">Le J-Top, faut que j'y vote !</div>
				<div class="pubImage"><img src="./pub1.png"></div>
				<div class="pubText">
					<span>
						Salut, c'est Josaudio, le roi de l'audio et de l'humour ! N'oublies pas d'aller voter pour tes clips préférés sur le site de Nolife et de regarder le classement chaque samedi à 19h, en direct (ou presque).
					</span>
				</div>
				<span class="retourAccueil">INDEX...........................<span class="pageNumber">100</span></span>
			</div>
			<div id="page_pub2" class="page">
				<div class="pubTitre">Offre spéciale nouveaux votants : le 1er slot à 10 cailloux !</div>
				<div class="pubImage"><img src="./pub2.png"></div>
				<div class="pubText">
					<span>
						Merci d'avoir rejoint les milliers de votants hebdomadaires pour le J-Top ! Pour te remercier de ton arrivée, nous te proposons une offre exceptionnelle : un slot de vote pour seulement 10 cailloux ! Ajoute un sixième clip à ta sélection et augmente ta puissance de vote.<br/><br/>
						<div style="font-size: 10px;">Quoi ? Le sixième slot est de base à 10 cailloux ? Chut, je sais, j'essaie juste d'avoir l'air sympa...</div>
					</span>
				</div>
				<span class="retourAccueil">INDEX...........................<span class="pageNumber">100</span></span>
			</div>
			<div id="page_pub3" class="page">
				<div class="pubTitre">Rejoins les petits amis de Nolife !</div>
				<div class="pubImage"><img src="./pub3.png"></div>
				<div class="pubText">
					<span>
						Envie de côtoyer d'autres téléspectateurs de la chaîne ? Adhères à l'ASSO des Téléspectateurs de Nolife !<br/><br/>
						Tu pourras ainsi participer à divers projets liés à Nolife ou ayant pour but de soutenir la chaîne.<br/><br/>
						Pour plus d'informations, rendez-vous sur www.atnl.fr.
					</span>
				</div>
				<span class="retourAccueil">INDEX...........................<span class="pageNumber">100</span></span>
			</div>
			<div id="page_pub4" class="page">
				<div class="pubTitre">noco, les vidéos de Nolife en ligne</div>
				<div class="pubImage"><img src="./pub4.png"></div>
				<div class="pubText">
					<span>
						Le catalogue Nolife sur noco, c'est la quasi-totalité des archives de Nolife.<br/><br/>
						Le catalogue Nolife sur noco, c'est plus de 11.000 vidéos à regarder à volonté.<br/><br/>
						Le catalogue Nolife sur noco, c'est seulement 5€ par mois et ça permet de soutenir la chaîne.<br/><br/>
						T'en veux encore ? Abonne-toi !<br/><br/>
						www.noco.tv
					</span>
				</div>
				<span class="retourAccueil">INDEX...........................<span class="pageNumber">100</span></span>
			</div>
			<div id="page_pub5" class="page">
				<div class="pubTitre">Aller au Japon pour pas cher avec toco toco travel</div>
				<div class="pubImage"><img src="./pub5.png"></div>
				<div class="pubText">
					<span>
						Envie d'aller au Japon mais tu n'as pas un rond ? Il te suffit de prendre tes pieds et d'aller voir des trucs avec des gens grâce à toco toco travel !<br/><br/>
						Prévoir une bonne paire de chaussures et un maillot de bain pour la traversée.
					</span>
				</div>
				<span class="retourAccueil">INDEX...........................<span class="pageNumber">100</span></span>
			</div>
			<div id="page_pub6" class="page">
				<div class="pubTitre">Moment de solitude ? medoc te donne toutes les bonnes adresses</div>
				<div class="pubImage"><img src="./pub6.png"></div>
				<div class="pubText">
					<span>
						Envie de chaleur mais tu es seul ce soir ? Pas de problème, medoc est là pour toi. Quelques soient tes fantasmes ou tes désirs les plus inavouables, medoc saura te provider de l'entertainment comme tu l'aimes.<br/><br/>
						1,34€/appel puis 0,34€/minute.<br/><br/>
						Pour votre premier appel, bénéficiez en cadeau de bienvenue d'un exemplaire de «Les 10 conseils pour bien effacer son historique», le best-seller de medoc.
					</span>
				</div>
				<span class="retourAccueil">INDEX...........................<span class="pageNumber">100</span></span>
			</div>
			<div id="page_credit" class="page">
				<div class="sommaireTitre">Crédits</div>
				<div class="newsContent">
					Bien sûr, Nolife ne va pas avoir un télétexte... C'est débile comme idée quand on y pense. Et puis faut aussi un peu de matos, que la chaîne n'a pas, pour envoyer un flux TXT.<br />
					<br />
					Ceci est un poisson d'avril. Tout le contenu (ou presque) provient du wiki, http://nolife-wiki.fr<br />
					<br />
					Préparé par Prof. Farnsworth et freeboite95, dans un délai extrêmement large :)
				</div>
				<span class="retourAccueil">INDEX...........................<span class="pageNumber">100</span></span>
			</div>
			<div id="page_nolife" class="page">
				<div class="sommaireTitre">Nolife</div>
				<div class="sommaireCol1">
					<span>LA CHAÎNE...............................<span class="pageNumber">111</span></span>
					<span>SAISONS.................................<span class="pageNumber">112</span></span>
					<span>PUBLICITÉ...............................<span class="pageNumber">113</span></span>
					<span>POCKET SHAMI............................<span class="pageNumber">114</span></span>
					<span>SITE....................................<span class="pageNumber">115</span></span>
					<span>MUSIQUES................................<span class="pageNumber">116</span></span>
					<span>CSA.....................................<span class="pageNumber">117</span></span>
					<span>LOGO....................................<span class="pageNumber">118</span></span>
				</div>
				<div class="sommaireCol2">
					<span>NOLIFE ∞................................<span class="pageNumber">121</span></span>
					<span>NOLIFE NO LACRYMA.......................<span class="pageNumber">122</span></span>
					<span>NOLIFE NO THEMA.........................<span class="pageNumber">123</span></span>
					<span>NOLIFE STORY............................<span class="pageNumber">124</span></span>
					<span>OTAKU2VICE..............................<span class="pageNumber">125</span></span>
					<span>ACID+ LE ANTOINETTE.....................<span class="pageNumber">126</span></span>
				</div>
				<span class="retourAccueil">INDEX...........................<span class="pageNumber">100</span></span>
			</div>
			<div id="page_noco" class="page">
				<div class="sommaireTitre">noco</div>
				<div class="sommaireCol1">
					<span>NOCO....................................<span class="pageNumber">131</span></span>
					<span>CATALOGUE NOLIFE........................<span class="pageNumber">132</span></span>
					<span>CATALOGUE GUARDIANS.....................<span class="pageNumber">133</span></span>
					<span>CATALOGUE LBL42.........................<span class="pageNumber">134</span></span>
					<span>CATALOGUE OLYDRI STUDIO.................<span class="pageNumber">135</span></span>
					<span>CATALOGUE TOKI MEDIA....................<span class="pageNumber">136</span></span>
					<span>CATALOGUE WAKANIM.......................<span class="pageNumber">137</span></span>
					<span>CATALOGUE WE PRODUCTIONS................<span class="pageNumber">138</span></span>
					<span>NOLIFE ONLINE...........................<span class="pageNumber">139</span></span>
				</div>
				<span class="retourAccueil">INDEX...........................<span class="pageNumber">100</span></span>
			</div>
			<div id="page_programmes" class="page">
				<div class="sommaireTitre">Programmes</div>
				<div class="sommaireCol1">
					<span>101%....................................<span class="pageNumber">210</span></span>
					<span>(VOUS SAVEZ) POURQUOI ON EST LÀ.........<span class="pageNumber">211</span></span>
					<span>1 MINUTE POUR PARLER DE.................<span class="pageNumber">212</span></span>
					<span>1D6.....................................<span class="pageNumber">213</span></span>
					<span>2 MINUTES POUR PARLER DE................<span class="pageNumber">214</span></span>
					<span>À LIRE, À VOIR..........................<span class="pageNumber">215</span></span>
					<span>BD BLOGUEURS............................<span class="pageNumber">216</span></span>
					<span>BIG BANG HUNTER.........................<span class="pageNumber">217</span></span>
					<span>BIG BUG HUNTER..........................<span class="pageNumber">218</span></span>
					<span>CATSUKA.................................<span class="pageNumber">219</span></span>
					<span>CE SOIR, J'AI RAID......................<span class="pageNumber">220</span></span>
					<span>COMPILER................................<span class="pageNumber">221</span></span>
					<span>CRITIQUE DE JEU.........................<span class="pageNumber">222</span></span>
					<span>CRUNCH TIME.............................<span class="pageNumber">223</span></span>
					<span>DEBUG MODE..............................<span class="pageNumber">224</span></span>
					<span>DOUBLE FACE.............................<span class="pageNumber">225</span></span>
					<span>FATAL MISSES &amp; PLASTIC GIRLS............<span class="pageNumber">226</span></span>
					<span>GAME CENTER.............................<span class="pageNumber">227</span></span>
					<span>HIDDEN PALACE...........................<span class="pageNumber">228</span></span>
					<span>LA FAUTE À L'ALGO.......................<span class="pageNumber">229</span></span>
					<span>LA MINUTE DU GEEK.......................<span class="pageNumber">230</span></span>
					<span>LES JEUX TÉLÉCHARGEABLES................<span class="pageNumber">231</span></span>
					<span>LES OUBLIÉS DE LA PLAYHISTOIRE..........<span class="pageNumber">232</span></span>
					<span>METAL MISSILE &amp; PLASTIC GUN.............<span class="pageNumber">233</span></span>
					<span>MON PLAN CULTE..........................<span class="pageNumber">234</span></span>
					<span>MON SOUVENIR............................<span class="pageNumber">235</span></span>
					<span>MONEY SHOT..............................<span class="pageNumber">236</span></span>
					<span>MOT DE SAISON...........................<span class="pageNumber">237</span></span>
					<span>NEWS....................................<span class="pageNumber">238</span></span>
					<span>NOCHAN..................................<span class="pageNumber">239</span></span>
				</div>
				<div class="sommaireCol2">
					<span>NOLIFE EMPLOI IRL.......................<span class="pageNumber">240</span></span>
					<span>OSCILLATIONS............................<span class="pageNumber">241</span></span>
					<span>OTO.....................................<span class="pageNumber">242</span></span>
					<span>OTO PLAY SAMPLE.........................<span class="pageNumber">243</span></span>
					<span>PÉRISCOPE...............................<span class="pageNumber">244</span></span>
					<span>PICO PICO...............................<span class="pageNumber">245</span></span>
					<span>PIXA....................................<span class="pageNumber">246</span></span>
					<span>REPORTAGE...............................<span class="pageNumber">247</span></span>
					<span>RETRO &amp; MAGIC...........................<span class="pageNumber">248</span></span>
					<span>ROADSTRIP LIGHT.........................<span class="pageNumber">249</span></span>
					<span>SMARTPHONES &amp; TABLETTES.................<span class="pageNumber">250</span></span>
					<span>SORTIES ANIMES ET MANGAS................<span class="pageNumber">251</span></span>
					<span>STAMINA.................................<span class="pageNumber">252</span></span>
					<span>SUPERPLAY...............................<span class="pageNumber">253</span></span>
					<span>TIPS....................................<span class="pageNumber">254</span></span>
					<span>TEMPS PERDU.............................<span class="pageNumber">255</span></span>
					<span>TÔKYÔ CAFÉ..............................<span class="pageNumber">256</span></span>
					<span>TOCO TOCO...............................<span class="pageNumber">257</span></span>
					<span>VERY HARD...............................<span class="pageNumber">258</span></span>
					<span>VUE SUBJECTIVE MINI.....................<span class="pageNumber">259</span></span>
					<span>101 PUR 100.............................<span class="pageNumber">260</span></span>
					<span>ACTU J-MUSIC............................<span class="pageNumber">261</span></span>
					<span>CE QU'IL NE FAUT PAS DIRE...............<span class="pageNumber">262</span></span>
					<span>CLUB TÉLÉ ACHIER........................<span class="pageNumber">263</span></span>
					<span>LE SAVIEZ-TU ? DE JEAN FOUTRE...........<span class="pageNumber">264</span></span>
					<span>L'HIPPODRÔLE DE NOLIFE..................<span class="pageNumber">265</span></span>
					<span>THE PLACE TO BE.........................<span class="pageNumber">266</span></span>
				</div>
				<span class="retourCategorie">SUITE...........................<span class="pageNumber">201</span></span>
				<span class="retourAccueil">INDEX...........................<span class="pageNumber">100</span></span>
			</div>
			<div id="page_programmes2" class="page">
				<div class="sommaireTitre">Programmes</div>
				<div class="sommaireCol1">
					<span class="sectionSommaire">PROGRAMMES MUSICAUX</span>
					<span>AMI AMI IDOL : HELLO! FRANCE............<span class="pageNumber">271</span></span>
					<span>C'EST MON VOTE..........................<span class="pageNumber">272</span></span>
					<span>J-TOP...................................<span class="pageNumber">273</span></span>
					<span>LE MAGAZINE DES INDIES..................<span class="pageNumber">274</span></span>
					<span>MOGURA MUSIQUE..........................<span class="pageNumber">275</span></span>
					<span>NOLIFE PLUGGED..........................<span class="pageNumber">276</span></span>
					<span>OTO EX..................................<span class="pageNumber">277</span></span>
					<span>WAR PIGS................................<span class="pageNumber">278</span></span>
					<span class="sectionSommaire">JEUX VIDÉO</span>
					<span>3 MINUTES PAS + POUR PARLER D'UN JEU 3+.<span class="pageNumber">281</span></span>
					<span>CHAUD TIME..............................<span class="pageNumber">282</span></span>
					<span>CHEZ MARCUS.............................<span class="pageNumber">283</span></span>
					<span>CLASSÉS 18+.............................<span class="pageNumber">284</span></span>
					<span>EXP.....................................<span class="pageNumber">285</span></span>
					<span>EXTRA LIFE..............................<span class="pageNumber">286</span></span>
					<span>FAQ.....................................<span class="pageNumber">287</span></span>
					<span>GAME LIFE...............................<span class="pageNumber">288</span></span>
					<span>HALL OF SHAME...........................<span class="pageNumber">289</span></span>
					<span>HARD CORNER.............................<span class="pageNumber">290</span></span>
					<span>JEU-TOP.................................<span class="pageNumber">291</span></span>
					<span>L'HEBDO JEU VIDÉO.......................<span class="pageNumber">292</span></span>
					<span>OTO PLAY................................<span class="pageNumber">293</span></span>
					<span>SKILL...................................<span class="pageNumber">294</span></span>
					<span>SUPERPLAY ULTIMATE......................<span class="pageNumber">295</span></span>
				</div>
				<div class="sommaireCol2">
					<span class="sectionSommaire">TALKS/NEWS</span>
					<span>56KAST..................................<span class="pageNumber">301</span></span>
					<span>ECRANS.FR LE PODCAST....................<span class="pageNumber">302</span></span>
					<span>J'IRAI LOLER SUR VOS TOMBES.............<span class="pageNumber">303</span></span>
					<span>PÉRISCOPE 2.............................<span class="pageNumber">304</span></span>
					<span class="sectionSommaire">BD/COMICS/MANGA</span>
					<span>CÔTÉ COMICS.............................<span class="pageNumber">311</span></span>
					<span>LE DUC COMICS...........................<span class="pageNumber">312</span></span>
					<span>ROADSTRIP...............................<span class="pageNumber">313</span></span>
					<span class="sectionSommaire">FICTION</span>
					<span>COURTS-MÉTRAGES.........................<span class="pageNumber">321</span></span>
					<span>FORMAT COURT............................<span class="pageNumber">322</span></span>
					<span>RÊVES ET CRIS...........................<span class="pageNumber">323</span></span>
					<span class="sectionSommaire">CULTURE JAPONAISE</span>
					<span>ESPRIT JAPON............................<span class="pageNumber">331</span></span>
					<span>JAPAN IN MOTION.........................<span class="pageNumber">332</span></span>
					<span>KIRA KIRA JAPON.........................<span class="pageNumber">333</span></span>
					<span>KYARY PAMYU PAMYU TELEBI JOHN !.........<span class="pageNumber">334</span></span>
					<span>KYÔ NO WANKO............................<span class="pageNumber">335</span></span>
					<span>NIHONGO GA DEKIMASU KA ?................<span class="pageNumber">336</span></span>
					<span class="sectionSommaire">HUMOUR</span>
					<span>L'INSTANT KAMIKAZE......................<span class="pageNumber">341</span></span>
					<span>LE GOLDEN SHOW..........................<span class="pageNumber">342</span></span>
					<span>PRÉLUDE EN LOL BÉMOL....................<span class="pageNumber">343</span></span>
					<span class="sectionSommaire">VIE DE LA CHAÎNE</span>
					<span>LE POINT SUR NOLIFE.....................<span class="pageNumber">351</span></span>
					<span>LES NEWS DE NOLIFE ONLINE...............<span class="pageNumber">352</span></span>
				</div>
				<span class="retourCategorie">SUITE...........................<span class="pageNumber">202</span></span>
				<span class="retourAccueil">INDEX...........................<span class="pageNumber">100</span></span>
			</div>
			<div id="page_programmes3" class="page">
				<div class="sommaireTitre">Programmes</div>
				<div class="sommaireCol1">
					<span class="sectionSommaire">PROGRAMMES GÉNÉRALISTES</span>
					<span>101% C'ÉTAIT MIEUX AVANT................<span class="pageNumber">361</span></span>
					<span>DOCUMENTAIRE............................<span class="pageNumber">362</span></span>
					<span>LE COIN DES ABONNÉS.....................<span class="pageNumber">363</span></span>
					<span>NOLIFE ONLINE COLLECTION................<span class="pageNumber">364</span></span>
					<span>OLDIES NOLIFE...........................<span class="pageNumber">365</span></span>
					<span>RANDOM ACCESS ARCHIVES..................<span class="pageNumber">366</span></span>
					<span>REPLAY VALUE............................<span class="pageNumber">367</span></span>
					<span>THE INCREDIBLE HORROR SHOW..............<span class="pageNumber">368</span></span>
					<span class="sectionSommaire">DIVERS</span>
					<span>À TABLE !...............................<span class="pageNumber">371</span></span>
					<span>COSTUGRALE..............................<span class="pageNumber">372</span></span>
					<span>COSTUME PLAYER..........................<span class="pageNumber">373</span></span>
					<span>LE JEU DU ***...........................<span class="pageNumber">374</span></span>
					<span>MANGE MON GEEK..........................<span class="pageNumber">375</span></span>
					<span>TEMPS RÉEL..............................<span class="pageNumber">376</span></span>
				</div>
				<div class="sommaireCol2">
					<span class="sectionSommaire">PROGRAMMES EXCEPTIONNELS</span>
					<span>100% LÉGAL..............................<span class="pageNumber">381</span></span>
					<span>ATTIC ATTIC ATTIC : AÏE AÏE AÏE.........<span class="pageNumber">382</span></span>
					<span>EXTRA NOLIFE............................<span class="pageNumber">383</span></span>
					<span>JEU-TOP ANNUEL..........................<span class="pageNumber">384</span></span>
					<span>J-MUSIC ULTRA COMBO PLAYLIST............<span class="pageNumber">385</span></span>
					<span>LE CALENDRIER DE L'AVENT APRÈS 101%.....<span class="pageNumber">386</span></span>
					<span>LE J-TOP DE L'ANNÉE.....................<span class="pageNumber">387</span></span>
					<span>LES CLIPS DE L'ANNÉE....................<span class="pageNumber">388</span></span>
					<span>LES IMMANQUABLES DE L'ANNÉE.............<span class="pageNumber">389</span></span>
					<span>LES IMMANQUABLES DE L'ANNÉE 2013........<span class="pageNumber">390</span></span>
					<span>LES IMMANQUABLES DE L'ANNÉE 2014........<span class="pageNumber">391</span></span>
					<span>LES SECONDES DE REMPLISSAGE.............<span class="pageNumber">392</span></span>
					<span>OTO REVERSE.............................<span class="pageNumber">393</span></span>
					<span>RUSH MODE...............................<span class="pageNumber">394</span></span>
					<span>SOIRÉE SPÉCIALE.........................<span class="pageNumber">395</span></span>
					<span>SPACE HYPER UFO MEDIA DJ................<span class="pageNumber">396</span></span>
					<span>THE GAME : L'EXPÉRIENCE.................<span class="pageNumber">397</span></span>
					<span>XMAS LIFE...............................<span class="pageNumber">398</span></span>
				</div>
				<span class="retourAccueil">INDEX...........................<span class="pageNumber">100</span></span>
			</div>
			<div id="page_series" class="page">
				<div class="sommaireTitre">Séries</div>
				<div class="sommaireCol1">
					<span>ANOTHER HERO............................<span class="pageNumber">411</span></span>
					<span>CRÉATION................................<span class="pageNumber">412</span></span>
					<span>DEVIL'SLAYER............................<span class="pageNumber">413</span></span>
					<span>FLANDER'S COMPANY.......................<span class="pageNumber">414</span></span>
					<span>FRANCE FIVE.............................<span class="pageNumber">415</span></span>
					<span>GEEK'S LIFE.............................<span class="pageNumber">416</span></span>
					<span>J'AI JAMAIS SU DIRE NON.................<span class="pageNumber">417</span></span>
					<span>KARATÉ BOY..............................<span class="pageNumber">418</span></span>
					<span>LA DERNIÈRE SÉRIE AVANT LA FIN DU MONDE.<span class="pageNumber">419</span></span>
					<span>LAZY PUPPY..............................<span class="pageNumber">420</span></span>
					<span>LE BLOG DE GAEA.........................<span class="pageNumber">421</span></span>
					<span>LE VISITEUR DU FUTUR....................<span class="pageNumber">422</span></span>
					<span>LES BLABLAGUES DE LAURENT-LAURENT.......<span class="pageNumber">423</span></span>
					<span>LES CONSEILS VIDÉO......................<span class="pageNumber">424</span></span>
					<span>METAL HURLANT CHRONICLES................<span class="pageNumber">425</span></span>
					<span>MON NOLIFE À MOI........................<span class="pageNumber">426</span></span>
					<span>NERDZ...................................<span class="pageNumber">427</span></span>
					<span>NONSÉRIE................................<span class="pageNumber">428</span></span>
					<span>NOOB....................................<span class="pageNumber">429</span></span>
					<span>PURGATOIRE..............................<span class="pageNumber">430</span></span>
					<span>PENDANT CE TEMPS... À LA RÉDAC..........<span class="pageNumber">431</span></span>
					<span>SPACE...................................<span class="pageNumber">432</span></span>
					<span>THE GUILD...............................<span class="pageNumber">433</span></span>
					<span>WARPZONE PROJECT........................<span class="pageNumber">434</span></span>
					<span>WIP.....................................<span class="pageNumber">435</span></span>
				</div>
				<span class="retourAccueil">INDEX...........................<span class="pageNumber">100</span></span>
			</div>
			<div id="page_animes" class="page">
				<div class="sommaireTitre">Animés</div>
				<div class="sommaireCol1">
					<span>CHIKYÛ SHÔJO ARJUNA.....................<span class="pageNumber">441</span></span>
					<span>ANIMATION RUNNER KUROMI.................<span class="pageNumber">442</span></span>
					<span>CITY HUNTER : BAY CITY WARS.............<span class="pageNumber">443</span></span>
					<span>CITY HUNTER : COMPLOT POUR 1.000.000 $..<span class="pageNumber">444</span></span>
					<span>RURÔNI KENSHIN : SEISÔHEN...............<span class="pageNumber">445</span></span>
					<span>RURÔNI KENSHIN : TSUIKUHEN..............<span class="pageNumber">446</span></span>
					<span>HAIBANE RENMEI - ALIES GRISES...........<span class="pageNumber">447</span></span>
					<span>BERSERK.................................<span class="pageNumber">448</span></span>
					<span>CITY HUNTER 3...........................<span class="pageNumber">449</span></span>
					<span>CITY HUNTER 91..........................<span class="pageNumber">450</span></span>
					<span>S-CRY-ED................................<span class="pageNumber">451</span></span>
					<span>TOP O NERAE! - GUNBUSTER................<span class="pageNumber">452</span></span>
					<span>TOP O NERAE 2! - DIEBUSTER..............<span class="pageNumber">453</span></span>
					<span>EYESHIELD 21............................<span class="pageNumber">454</span></span>
					<span>KARE KANO - ENTRE ELLE ET LUI...........<span class="pageNumber">455</span></span>
					<span>WINDY TALES.............................<span class="pageNumber">456</span></span>
					<span>FULICULI................................<span class="pageNumber">457</span></span>
					<span>RAHXEPHON...............................<span class="pageNumber">458</span></span>
					<span>LA MÉLANCOLIE DE HARUHI SUZUMIYA........<span class="pageNumber">459</span></span>
					<span>QUE SA VOLONTÉ SOIT FAITE...............<span class="pageNumber">460</span></span>
					<span>PARANOIA AGENT..........................<span class="pageNumber">461</span></span>
					<span>SAMURAI CHAMPLOO........................<span class="pageNumber">462</span></span>
					<span>INNOCENT VENUS..........................<span class="pageNumber">463</span></span>
					<span>THE TOWER OF DRUAGA.....................<span class="pageNumber">464</span></span>
					<span>EDEN OF THE EAST........................<span class="pageNumber">465</span></span>
					<span>ANOTHER.................................<span class="pageNumber">466</span></span>
					<span>BLACK★ROCK SHOOTER......................<span class="pageNumber">467</span></span>
					<span>KIDS ON THE SLOPE.......................<span class="pageNumber">468</span></span>
					<span>DEADMAN WONDERLAND......................<span class="pageNumber">469</span></span>
				</div>
				<div class="sommaireCol2">
					<span>FLAG....................................<span class="pageNumber">470</span></span>
					<span>JYU OH SEI..............................<span class="pageNumber">471</span></span>
					<span>PRINCESS JELLYFISH......................<span class="pageNumber">472</span></span>
					<span>C-CONTROL...............................<span class="pageNumber">473</span></span>
					<span>FRACTALE................................<span class="pageNumber">474</span></span>
					<span>CASSHERN SINS...........................<span class="pageNumber">475</span></span>
					<span>BAKEMONOGATARI..........................<span class="pageNumber">476</span></span>
					<span>BLOOD LAD...............................<span class="pageNumber">477</span></span>
					<span>NADIA, LE SECRET DE L'EAU BLEUE.........<span class="pageNumber">478</span></span>
					<span>TSURITAMA...............................<span class="pageNumber">479</span></span>
					<span>FREE!...................................<span class="pageNumber">480</span></span>
					<span>NOIR....................................<span class="pageNumber">481</span></span>
					<span>GUNDAM G NO RECONGUISTA.................<span class="pageNumber">482</span></span>
					<span>WOLF'S RAIN.............................<span class="pageNumber">483</span></span>
					<span>KOITABI.................................<span class="pageNumber">484</span></span>
				</div>
				<span class="retourAccueil">INDEX...........................<span class="pageNumber">100</span></span>
			</div>
			<div id="page_staff1" class="page">
				<div class="sommaireTitre">L'équipe</div>
				<div class="sommaireCol1">
					<span>SÉBASTIEN RUCHET........................<span class="pageNumber">502</span></span>
					<span>ALEX PILOT..............................<span class="pageNumber">503</span></span>
					<span>ASAOKA SUZUKA...........................<span class="pageNumber">504</span></span>
					<span>CYRIL LAMBIN............................<span class="pageNumber">505</span></span>
					<span>KÉVIN CICUREL (MOGURI)..................<span class="pageNumber">506</span></span>
					<span class="sectionSommaire">ANIMATEURS/JOURNALISTES</span>
					<span>ANNE FERRERO............................<span class="pageNumber">511</span></span>
					<span>AURORE..................................<span class="pageNumber">512</span></span>
					<span>AYMERIC RAMANAKASINA....................<span class="pageNumber">513</span></span>
					<span>CAMILLE GÉVAUDAN........................<span class="pageNumber">514</span></span>
					<span>DAVY MOURIER............................<span class="pageNumber">515</span></span>
					<span>EMMANUEL PETTINI (CHRON)................<span class="pageNumber">516</span></span>
					<span>ERWAN CARIO.............................<span class="pageNumber">517</span></span>
					<span>FLORENT GORGES..........................<span class="pageNumber">518</span></span>
					<span>FRÉDÉRIC HOSTEING.......................<span class="pageNumber">519</span></span>
					<span>JEAN-MARC IMBERT........................<span class="pageNumber">520</span></span>
					<span>JOSAUDIO................................<span class="pageNumber">521</span></span>
					<span>JULIEN PIROU............................<span class="pageNumber">522</span></span>
					<span>LILIAN MICHELON.........................<span class="pageNumber">523</span></span>
					<span>MARC AGUESSE (TSUKA)....................<span class="pageNumber">524</span></span>
					<span>MARCUS..................................<span class="pageNumber">525</span></span>
					<span>MEDOC...................................<span class="pageNumber">526</span></span>
					<span>NICOLAS ROBIN...........................<span class="pageNumber">527</span></span>
					<span>PAROTAKU................................<span class="pageNumber">528</span></span>
					<span>PASCALINE...............................<span class="pageNumber">529</span></span>
					<span>PAUL BOURRIÈRES.........................<span class="pageNumber">530</span></span>
					<span>SIRONIMO................................<span class="pageNumber">531</span></span>
					<span>SLIMANE-BAPTISTE BERHOUN................<span class="pageNumber">532</span></span>
				</div>
				<div class="sommaireCol2">
					<span class="sectionSommaire">TECHNIQUE</span>
					<span>ANGÉLA NICHON...........................<span class="pageNumber">541</span></span>
					<span>AURÉLIEN LOUVET.........................<span class="pageNumber">544</span></span>
					<span>BRIAN MARTIN............................<span class="pageNumber">543</span></span>
					<span>GENEVIÈVE DOANG.........................<span class="pageNumber">544</span></span>
					<span>GUILLAUME BOSCHINI......................<span class="pageNumber">545</span></span>
					<span>JONATHAN A. CASSES......................<span class="pageNumber">546</span></span>
					<span>LAURINE BOLLON..........................<span class="pageNumber">547</span></span>
					<span>OLIVIER ROCQUES.........................<span class="pageNumber">548</span></span>
					<span>JULIEN LEVAISQUE (RADIGO)...............<span class="pageNumber">534</span></span>
					<span>RUDDY POMAREDE..........................<span class="pageNumber">541</span></span>
					<span>RÉMY ARGAUD.............................<span class="pageNumber">544</span></span>
				</div>
				<span class="retourCategorie">SUITE...........................<span class="pageNumber">501</span></span>
				<span class="retourAccueil">INDEX...........................<span class="pageNumber">100</span></span>
			</div>
			<div id="page_staff2" class="page">
				<div class="sommaireTitre">L'équipe</div>
				<div class="sommaireCol1">
					<span class="sectionSommaire">ANCIENS</span>
					<span>ANH HOÀ TRUONG..........................<span class="pageNumber">551</span></span>
					<span>BENOÎT LEGRAND..........................<span class="pageNumber">552</span></span>
					<span>BENZAIE.................................<span class="pageNumber">553</span></span>
					<span>CAROLINE SEGARRA........................<span class="pageNumber">554</span></span>
					<span>CLAIRE..................................<span class="pageNumber">555</span></span>
					<span>CLÉMENT MAURIN..........................<span class="pageNumber">556</span></span>
					<span>DAMDAM..................................<span class="pageNumber">557</span></span>
					<span>DARKWAM.................................<span class="pageNumber">558</span></span>
					<span>DIDIER RICHARD..........................<span class="pageNumber">559</span></span>
					<span>DR LAKAV................................<span class="pageNumber">560</span></span>
					<span>JULIEN LEVAISQUE (RADIGO)...............<span class="pageNumber">561</span></span>
					<span>MACHA...................................<span class="pageNumber">562</span></span>
					<span>MATHILDE WASILEWSKI.....................<span class="pageNumber">563</span></span>
					<span>MIMA....................................<span class="pageNumber">564</span></span>
					<span>MR POULPE...............................<span class="pageNumber">565</span></span>
					<span>PHILIPPE NÈGRE..........................<span class="pageNumber">566</span></span>
					<span>PIERRE-ALEXANDRE CONTE..................<span class="pageNumber">567</span></span>
					<span>RÉMY ARGAUD.............................<span class="pageNumber">568</span></span>
					<span>RUDDY POMAREDE..........................<span class="pageNumber">569</span></span>
					<span>SÉBASTIEN CIOT..........................<span class="pageNumber">570</span></span>
					<span>SYLVAIN DREYER (ZED)....................<span class="pageNumber">571</span></span>
					<span>THIERRY FALCOZ..........................<span class="pageNumber">572</span></span>
					<span>THOMAS GUITARD..........................<span class="pageNumber">573</span></span>
				</div>
				<div class="sommaireCol2">
				</div>
				<span class="retourAccueil">INDEX...........................<span class="pageNumber">100</span></span>
			</div>
			<div id="page_grille" class="page">
				<div class="sommaireTitre">Grille des programmes</div>
				<div class="sommaireCol1">
					<span>AUJOURD'HUI.............................<span class="pageNumber">601</span></span>
					<span>DEMAIN..................................<span class="pageNumber">611</span></span>
				</div>
				<span class="retourAccueil">INDEX...........................<span class="pageNumber">100</span></span>
			</div>
			<div id="page_wiki" class="page">
				<div id="wikiTitre"></div>
				<div id="wikiContent">
				</div>
				<span class="retourCategorie" id="retourCategorieWiki"></span>
				<span class="retourAccueil">INDEX...........................<span class="pageNumber">100</span></span>
			</div>
		</div>
		<script type="text/javascript" src="teletexte.js"></script>
		<!-- Piwik -->
		<script type="text/javascript">
			var _paq = _paq || [];
			_paq.push(['trackPageView']);
			_paq.push(['enableLinkTracking']);
			(function() {
				var u="//stats.commun.nolife-wiki.fr/";
				_paq.push(['setTrackerUrl', u+'piwik.php']);
				_paq.push(['setSiteId', 12]);
				var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
				g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
			})();
		</script>
		<noscript><p><img src="//stats.commun.nolife-wiki.fr/piwik.php?idsite=12" style="border:0;" alt="" /></p></noscript>
		<!-- End Piwik Code -->
	</body>
</html>
